import java.awt.Point;
import java.util.ArrayList;

import processing.core.*;
import processing.opengl.*;
import SimpleOpenNI.*;
import hypermedia.video.*;

public class UserBlob extends PApplet {

	SimpleOpenNI context;
	OpenCV opencv;

	int[] depth;
	int maxDepth = 2100;
	int minDepth = 100;
	final int IMG_WIDTH = 640;
	final int IMG_HEIGHT = 480;
	ArrayList<Moment> moments;
	ArrayList<Block> blocks;
	int interval = 0;
	int blockSize = 60;

	public static void main(String[] args) {
		PApplet.main(new String[] { UserBlob.class.getName() });
	}

	public void setup() {
		size(1280, 480, P3D);
		background(0);

		context = new SimpleOpenNI(this);
		opencv = new OpenCV(this);
		opencv.capture(IMG_WIDTH, IMG_HEIGHT);
		opencv.allocate(IMG_WIDTH, IMG_HEIGHT);

		// enable depthMap generation
		if (context.enableDepth() == false) {
			println("Can't open the depthMap, maybe the camera is not connected!");
			exit();
			return;
		}

		depth = new int[640 * 480];
		moments = new ArrayList<Moment>();
		blocks = new ArrayList<Block>();

		for (int x = 0; x < IMG_WIDTH; x += blockSize) {
			for (int y = 0; y < IMG_HEIGHT; y += blockSize) {
				fill(random(255));
				rect(x, y, blockSize, blockSize);
				Block b = new Block(this, x, y);
				blocks.add(b);
			}
		}
	}

	public void draw() {
		background(0);
		interval++;
		context.update();
		PImage depthImage = context.depthImage();
		image(depthImage, 640, 0);

		context.depthMap(depth);
		drawBlobs(depth);

		drawGrid();
	}

	public void drawGrid() {
		
		for (int j = 0; j < blocks.size(); j++) {
			Block b = blocks.get(j);
			stroke(0, 255, 0);
			fill(255, 100);
			rect(IMG_WIDTH + b.x, b.y, blockSize, blockSize);
			noStroke();
			fill(0, 0, 255);
			text(b.moments.size(),IMG_WIDTH + b.x + blockSize/2, b.y +blockSize/2);
			
		}
	}

	public void drawBlobs(int[] depth) {
		PImage img = createImage(IMG_WIDTH, IMG_HEIGHT, RGB);
		img.loadPixels();
		for (int x = 0; x < IMG_WIDTH; x++) {
			for (int y = 0; y < IMG_HEIGHT; y++) {
				int pixelNumber = y * IMG_WIDTH + x;
				if (depth[pixelNumber] != 0 && depth[pixelNumber] < maxDepth
						&& depth[pixelNumber] > minDepth) {
					img.pixels[pixelNumber] = color(255, 0, 0);
				}
			}
		}
		img.updatePixels();
		image(img, IMG_WIDTH, 0);

		opencv.copy(img);
		opencv.blur(1, 11);
		// find blobs
		Blob[] blobs = opencv
				.blobs(2000, IMG_WIDTH * IMG_HEIGHT / 2, 100, false);

		// draw blob results
		//for (int i = 0; i < blobs.length; i++) {
		for (int i = 0; i< constrain(blobs.length, 0, 1); i++) {
			stroke(0, 255, 0);
			fill(255);
			beginShape();
			for (int j = 0; j < blobs[i].points.length; j++) {
				vertex(blobs[i].points[j].x + IMG_WIDTH, blobs[i].points[j].y);
			}
			endShape(CLOSE);

			Point centroid = blobs[i].centroid;
			noStroke();
			fill(255, 0, 0);
			ellipse(centroid.x + IMG_WIDTH, centroid.y, 10, 10);

			for (int j = 0; j < blocks.size(); j++) {
				Block b = blocks.get(j);
				if (centroid.x >= b.x && centroid.x <= b.x + blockSize
						&& centroid.y >= b.y && centroid.y <= b.y + blockSize) {
					
					PImage randomImage = b.getRandomImage();
					if (randomImage!=null) {
						image(randomImage, 0, 0);
					}
					if (interval >= 20) {
						interval = 0;
						opencv.read();
						PImage camImage = opencv.image();
						
						PImage backupImage = createImage(camImage.width, camImage.height, RGB);
						backupImage.copy(camImage, 0, 0, camImage.width, camImage.height,0, 0, camImage.width, camImage.height);
						
						Moment m = new Moment(this, centroid.x, centroid.y,
								backupImage);

						b.addMoment(m);
					}
				}
			}
		}
	}

}
