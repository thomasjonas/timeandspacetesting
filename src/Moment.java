import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import processing.core.PApplet;
import processing.core.PImage;


public class Moment {
	
	PApplet parent;
	Date date;
	int x;
	int y;
	String filename;
	
	Moment(PApplet p, int xPos, int yPos, PImage img) {
		this.parent = p;
		this.x = xPos;
		this.y = yPos;
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
		this.date = new Date();
		this.filename = this.x+"_"+this.y+"_"+dateFormat.format(this.date)+".jpg";
		img.save("images/"+filename);
	}
}
