import processing.core.PApplet;
import processing.core.PImage;
import java.util.ArrayList;

public class Block {
	
	PApplet parent;
	int x;
	int y;
	ArrayList<Moment> moments;
	
	Block(PApplet p, int x, int y) {
		this.parent = p;
		this.x = x;
		this.y = y;
		
		moments = new ArrayList<Moment>();
	}
	
	public void addMoment(Moment m) {
		System.out.println("This is the moment");
		moments.add(m);
	}
	
	public PImage getRandomImage() {
		PImage img = null;
		if(moments.size() > 0) {
			int num =(int) ( Math.random() * moments.size());
			Moment m = moments.get(num);
			String filename = m.filename;
			img = parent.loadImage("images/"+filename);
		}
		return img;
	}

}
